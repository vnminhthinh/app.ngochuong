package com.example.minhthinh.myapplication.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhthinh.myapplication.Adapter.RecyclerViewBeautifyAdapter;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewEventsAdapter;
import com.example.minhthinh.myapplication.Model.Beautify;
import com.example.minhthinh.myapplication.Model.Events;
import com.example.minhthinh.myapplication.Model.Knowledge;
import com.example.minhthinh.myapplication.Model.Posts;
import com.example.minhthinh.myapplication.Model.Service;
import com.example.minhthinh.myapplication.R;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewKnowledgeAdapter;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewServiceAdapter;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewServiceListAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 12/12/18.
 */

public class FragmentService extends Fragment {
    private View mView;
    private RecyclerView recyclerViewEvents;
    private RecyclerView recyclerViewServiceList;
    private RecyclerView recyclerView_service;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.LayoutManager layoutManager1;
    private List<Events> eventsList = new ArrayList<>();
    private List<Service> serviceList = new ArrayList<>();
    private List<Service> services = new ArrayList<>();
    private RecyclerViewEventsAdapter adapterEvents;
    private RecyclerViewServiceAdapter adapterService;
    private RecyclerViewServiceListAdapter adapter_service_list;
    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_service, container, false);
        getActivity().setTitle("Dịch Vụ");
        database = FirebaseDatabase.getInstance();
        recyclerViewEvents = mView.findViewById(R.id.recyclerView_events);
        recyclerViewServiceList = mView.findViewById(R.id.recyclerView_service_list);
        recyclerView_service = mView.findViewById(R.id.recyclerView_service);
        layoutManager = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManager1 = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        setupRecyclerViewEvents();
        setUpRecyclerViewServiceList();
        setUpRecyclerViewService();
        return mView;

    }

    private void setupRecyclerViewEvents() {
        recyclerViewEvents.setLayoutManager(layoutManager);
        myRef = database.getReference("posts").child("mostViewEvents");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Events events = new Events();
                        events.setId(data.child("id").getValue().toString());
                        events.setTitle(data.child("title").getValue().toString());
                        events.setImg(data.child("img").getValue().toString());
                        eventsList.add(events);
                    }
                    if (eventsList.size() != 0) {
                        adapterEvents = new RecyclerViewEventsAdapter(mView.getContext(), eventsList);
                        recyclerViewEvents.setAdapter(adapterEvents);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });

    }

    private void setUpRecyclerViewServiceList() {
        recyclerViewServiceList.setLayoutManager(new GridLayoutManager(mView.getContext(), 2));
        myRef = database.getReference("posts").child("mostViewServiceList");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Service service = new Service();
                        service.setId(data.child("id").getValue().toString());
                        service.setTitle(data.child("title").getValue().toString());
                        service.setImg(data.child("img").getValue().toString());
                        serviceList.add(service);
                    }
                    if (serviceList.size() != 0) {
                        adapter_service_list = new RecyclerViewServiceListAdapter(mView.getContext(), serviceList);
                        recyclerViewServiceList.setAdapter(adapter_service_list);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });


    }

    private void setUpRecyclerViewService() {
        recyclerView_service.setLayoutManager(layoutManager1);
        myRef = database.getReference("posts").child("mostViewService");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Service service = new Service();
                        service.setId(data.child("id").getValue().toString());
                        service.setTitle(data.child("title").getValue().toString());
                        service.setImg(data.child("img").getValue().toString());
                        services.add(service);
                    }
                    if (services.size() != 0) {
                        adapterService = new RecyclerViewServiceAdapter(mView.getContext(), services);
                        recyclerView_service.setAdapter(adapterService);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });


    }

}
