package com.example.minhthinh.myapplication.Fragment;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.minhthinh.myapplication.Model.Beautify;
import com.example.minhthinh.myapplication.Model.Knowledge;
import com.example.minhthinh.myapplication.Model.Posts;
import com.example.minhthinh.myapplication.R;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewBeautifyAdapter;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewKnowledgeAdapter;
import com.example.minhthinh.myapplication.Adapter.RecyclerViewPostsAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by minhthinh on 11/19/18.
 */

public class FragmentKnowledge extends Fragment {
    private View mView;
    private ImageView imageView;
    private RecyclerView recyclerView;
    private RecyclerView recyclerView_beautify;
    private RecyclerView recyclerView_knowledge;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.LayoutManager layoutManager1;
    private RecyclerView.LayoutManager layoutManager2;
    private List<Posts> postsList = new ArrayList<>();
    private List<Beautify> beautifies = new ArrayList<>();
    private List<Knowledge> knowledgeList = new ArrayList<>();
    private RecyclerViewPostsAdapter adapter;
    private RecyclerViewBeautifyAdapter adapter_beautify;
    private RecyclerViewKnowledgeAdapter adapterKnowledge;

    private FirebaseDatabase database;
    private DatabaseReference myRef;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_knowledge, container, false);
        getActivity().setTitle("Kiến thức làm đẹp");
        database = FirebaseDatabase.getInstance();
        imageView = mView.findViewById(R.id.img);
        recyclerView = mView.findViewById(R.id.recyclerView);
        recyclerView_beautify = mView.findViewById(R.id.recyclerView1);
        recyclerView_knowledge = mView.findViewById(R.id.recyclerView_knowledge);
        layoutManager = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        layoutManager1 = new LinearLayoutManager(mView.getContext(), LinearLayoutManager.HORIZONTAL, false);
        setUpRecyclerViewKnowledge();
        setUpRecyclerViewBeautify();
        setupRecyclerViewPosts();
        return mView;
    }

    private void setupRecyclerViewPosts() {
        recyclerView.setLayoutManager(layoutManager);
        myRef = database.getReference("posts").child("mostViewPosts");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Posts posts = new Posts();
                        posts.setId(data.child("id").getValue().toString());
                        posts.setTitle(data.child("title").getValue().toString());
                        posts.setImg(data.child("img").getValue().toString());
                        posts.setLike(data.child("like").getValue().toString());
                        posts.setShare(data.child("share").getValue().toString());
                        posts.setView(data.child("view").getValue().toString());
                        posts.setDate(data.child("date").getValue().toString());
                        postsList.add(posts);
                    }
                    if (postsList.size() != 0) {
                        adapter = new RecyclerViewPostsAdapter(mView.getContext(), postsList);
                        recyclerView.setAdapter(adapter);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });

    }

    private void setUpRecyclerViewBeautify() {
        recyclerView_beautify.setLayoutManager(layoutManager1);
        myRef = database.getReference("posts").child("mostViewBeautify");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Beautify beautify = new Beautify();
                        beautify.setId(data.child("id").getValue().toString());
                        beautify.setTitle(data.child("title").getValue().toString());
                        beautify.setImg(data.child("img").getValue().toString());
                        beautifies.add(beautify);
                    }
                    if (beautifies.size() != 0) {
                        adapter_beautify = new RecyclerViewBeautifyAdapter(mView.getContext(), beautifies);
                        recyclerView_beautify.setAdapter(adapter_beautify);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });


    }

    private void setUpRecyclerViewKnowledge() {
        recyclerView_knowledge.setLayoutManager(new GridLayoutManager(mView.getContext(), 2));
        myRef = database.getReference("posts").child("mostViewKnowledge");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                        Knowledge knowledge = new Knowledge();
                        knowledge.setId(data.child("id").getValue().toString());
                        knowledge.setTitle(data.child("title").getValue().toString());
                        knowledge.setImg(data.child("img").getValue().toString());
                        knowledgeList.add(knowledge);
                    }
                    if (knowledgeList.size() != 0) {
                        adapterKnowledge = new RecyclerViewKnowledgeAdapter(mView.getContext(), knowledgeList);
                        recyclerView_knowledge.setAdapter(adapterKnowledge);
                    } else {
                        Log.d("BBB", "sai");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("BBB", "on cancel");
            }
        });


    }
}
