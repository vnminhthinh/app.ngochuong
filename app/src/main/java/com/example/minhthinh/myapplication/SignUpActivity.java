package com.example.minhthinh.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.minhthinh.myapplication.Model.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.input_email_sign_up)
    EditText inputEmail;
    @BindView(R.id.input_password_sign_up)
    EditText inputPassword;
    @BindView(R.id.btn_signup)
    Button mButtonSignUp;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String userId;
    private User user;
    private String email, password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        database = FirebaseDatabase.getInstance();
        mButtonSignUp.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_signup:
                email = inputEmail.getText().toString().trim();
                password = inputPassword.getText().toString().trim();
                myRef = database.getReference("user");
                userId = myRef.push().getKey();

                user = new User(email,password );
                myRef.child(userId).setValue(user);
                startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                Toast.makeText(this, "Sign Up success", Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }
}
