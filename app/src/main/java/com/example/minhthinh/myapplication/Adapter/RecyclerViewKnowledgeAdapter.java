package com.example.minhthinh.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhthinh.myapplication.Model.Knowledge;
import com.example.minhthinh.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 12/11/18.
 */

public class RecyclerViewKnowledgeAdapter extends RecyclerView.Adapter<RecyclerViewKnowledgeAdapter.ViewHolder> {
    private Context context;
    private List<Knowledge> knowledges = new ArrayList<>();

    public RecyclerViewKnowledgeAdapter(Context context, List<Knowledge> knowledges) {
        this.context = context;
        this.knowledges = knowledges;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_knowledge, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context)
                .load(knowledges.get(position).getImg())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);
        holder.textTitle.setText(knowledges.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return knowledges.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_knowledge);
            textTitle = itemView.findViewById(R.id.text_title);

        }
    }
}
