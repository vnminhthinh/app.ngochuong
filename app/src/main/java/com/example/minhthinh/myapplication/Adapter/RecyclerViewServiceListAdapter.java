package com.example.minhthinh.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhthinh.myapplication.Model.Beautify;
import com.example.minhthinh.myapplication.Model.Service;
import com.example.minhthinh.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 12/12/18.
 */

public class RecyclerViewServiceListAdapter extends RecyclerView.Adapter<RecyclerViewServiceListAdapter.viewHolder> {
    private Context mContext;
    private List<Service> serviceList= new ArrayList<>();

    public RecyclerViewServiceListAdapter(Context mContext, List<Service> serviceList) {
        this.mContext = mContext;
        this.serviceList = serviceList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView= layoutInflater.inflate(R.layout.item_service_list,parent,false);
        return new viewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        Picasso.with(mContext)
                .load(serviceList.get(position).getImg())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);
        holder.textTitle.setText(serviceList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textTitle;
        public viewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_service_list);
            textTitle = itemView.findViewById(R.id.text_service_list);
        }
    }
}
