package com.example.minhthinh.myapplication.Model;

/**
 * Created by minhthinh on 12/12/18.
 */

public class Posts {
    private String id;
    private String img;
    private String title;
    private String view;
    private String share;
    private String like;
    private String date;

    public Posts() {
    }

    public Posts(String id, String img, String title, String view, String share, String like, String date) {
        this.id = id;
        this.img = img;
        this.title = title;
        this.view = view;
        this.share = share;
        this.like = like;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
