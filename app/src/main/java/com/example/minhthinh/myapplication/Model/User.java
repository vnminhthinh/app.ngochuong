package com.example.minhthinh.myapplication.Model;

/**
 * Created by minhthinh on 11/21/18.
 */

public class User {
    public String name;
    public String email;

    public User() {
    }

    public User(String email, String name) {
        this.email = email;
        this.name = name;

    }
}
