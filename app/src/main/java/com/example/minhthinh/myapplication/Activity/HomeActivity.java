package com.example.minhthinh.myapplication.Activity;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.example.minhthinh.myapplication.Fragment.BottomNavigationViewHelper;
import com.example.minhthinh.myapplication.Fragment.FragmentA;
import com.example.minhthinh.myapplication.Fragment.FragmentKnowledge;
import com.example.minhthinh.myapplication.Fragment.FragmentNotification;
import com.example.minhthinh.myapplication.Fragment.FragmentProfile;
import com.example.minhthinh.myapplication.Fragment.FragmentService;
import com.example.minhthinh.myapplication.R;

public class HomeActivity extends AppCompatActivity {

    private boolean doubleBackToExitPressedOnce= false;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setUpToolBar();
        setUpMenu();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    new FragmentKnowledge()).commit();
        }

    }

    private void setUpToolBar() {
        toolbar=findViewById(R.id.toolbar);
        setTitle("Kiến thức làm đẹp");
        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        }
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
    }



    private void setUpMenu() {
        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottomNavView_Bar);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()){
                    case R.id.nav_home:
                        selectedFragment = new FragmentKnowledge();
                        break;

                    case R.id.nav_service:
                        selectedFragment = new FragmentService();
                        break;

                    case R.id.nav_notification:
                        selectedFragment = new FragmentNotification();
                        break;

                    case R.id.nav_chat:
                        selectedFragment = new FragmentA();
                        break;

                    case R.id.nav_profile:

                        selectedFragment = new FragmentProfile();
                        break;
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();

                return true;
            }
        });
    }


    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Vui lòng nhấp vào BACK lần nữa để thoát", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
