package com.example.minhthinh.myapplication.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minhthinh.myapplication.R;

/**
 * Created by minhthinh on 12/12/18.
 */

public class FragmentNotification extends Fragment{
    private View mView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.layout_notification, container, false);
        getActivity().setTitle("Thông báo");
        return mView;
    }
}
