package com.example.minhthinh.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhthinh.myapplication.Model.Beautify;
import com.example.minhthinh.myapplication.Model.Events;
import com.example.minhthinh.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 12/12/18.
 */

public class RecyclerViewEventsAdapter extends RecyclerView.Adapter<RecyclerViewEventsAdapter.viewHolder> {
    private Context mContext;
    private List<Events> eventsList= new ArrayList<>();

    public RecyclerViewEventsAdapter(Context mContext, List<Events> eventsList) {
        this.mContext = mContext;
        this.eventsList = eventsList;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView= layoutInflater.inflate(R.layout.item_events,parent,false);
        return new viewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, int position) {
        Picasso.with(mContext)
                .load(eventsList.get(position).getImg())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);
        holder.textTitle.setText(eventsList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textTitle;

        public viewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_events);
            textTitle = itemView.findViewById(R.id.text_title_events);


        }
    }
}
