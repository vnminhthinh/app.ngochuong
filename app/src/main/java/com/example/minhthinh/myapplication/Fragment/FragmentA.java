package com.example.minhthinh.myapplication.Fragment;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.minhthinh.myapplication.R;
import com.example.minhthinh.myapplication.Model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 11/19/18.
 */

public class FragmentA extends Fragment implements View.OnClickListener {
    private View mView;
    TextView mTextView;
    Button mButtonAdd;
    Button mButtonGet;
    Button mButtonGetList;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String userId;
    private User user;
    private FirebaseAuth auth;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_a, container, false);
        mTextView = mView.findViewById(R.id.textview);
        mButtonAdd = mView.findViewById(R.id.btn_add);
        mButtonGet = mView.findViewById(R.id.btn_get);
        mButtonGetList = mView.findViewById(R.id.btn_get_list);
        database = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();
        mButtonAdd.setOnClickListener(this);
        mButtonGet.setOnClickListener(this);
        mButtonGetList.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_add:
                myRef = database.getReference("user");
                userId = myRef.push().getKey();

                user = new User("vuongthinh785@gmail.com", "minhthinh");
                myRef.child(userId).setValue(user);

                break;
            case R.id.btn_get:
                myRef.child(userId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        Log.d("BBB", "Name " + user.name + " email" + user.email);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                break;
            case R.id.btn_get_list:

                myRef = database.getReference("user");
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            List<User> users = new ArrayList<>();
                            for (DataSnapshot data : dataSnapshot.getChildren()) {
                                user = new User();
                                user.email = data.child("email").getValue().toString();
                                user.name = data.child("name").getValue().toString();
                                users.add(user);
                            }
                            if (users.size() != 0) {
                                Log.d("BBB", "size list  " + users.size());
                            } else {
                                Log.d("BBB", "error");
                            }
                        } else {
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

//                auth.sendPasswordResetEmail("vuongthinh785@gmail.com")
//
//                        .addOnCompleteListener(new OnCompleteListener<Void>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Void> task) {
//                                if (task.isSuccessful()) {
//
//                                    Toast.makeText(getContext(), "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    Toast.makeText(getContext(), "Failed to send reset email!", Toast.LENGTH_SHORT).show();
//                                }
//
//                            }
//                        });
                break;


        }
    }
}
