package com.example.minhthinh.myapplication.Model;

/**
 * Created by minhthinh on 12/12/18.
 */

public class Events {
    private String id;
    private String img;
    private String title;

    public Events() {
    }

    public Events(String id, String img, String title) {
        this.id = id;
        this.img = img;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
