package com.example.minhthinh.myapplication.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhthinh.myapplication.Model.Posts;
import com.example.minhthinh.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minhthinh on 12/12/18.
 */

public class RecyclerViewPostsAdapter extends RecyclerView.Adapter<RecyclerViewPostsAdapter.ViewHolder>{
    private Context context;
    private List<Posts> postsList = new ArrayList<>();

    public RecyclerViewPostsAdapter(Context context, List<Posts> postsList) {
        this.context = context;
        this.postsList = postsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View itemView = layoutInflater.inflate(R.layout.item_posts, parent, false);
        return new RecyclerViewPostsAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context)
                .load(postsList.get(position).getImg())
                .placeholder(R.mipmap.ic_launcher)
                .into(holder.imageView);
        holder.textTitle.setText(postsList.get(position).getTitle());
        holder.textLike.setText(postsList.get(position).getLike());
        holder.textShare.setText(postsList.get(position).getShare());
        holder.textView.setText(postsList.get(position).getView());
        holder.textDate.setText(postsList.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textTitle, textView, textLike, textShare, textDate;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img);
            textTitle = itemView.findViewById(R.id.textTitle);
            textLike = itemView.findViewById(R.id.textlike);
            textView = itemView.findViewById(R.id.textview);
            textShare = itemView.findViewById(R.id.textshare);
            textDate = itemView.findViewById(R.id.textDate);

        }
    }
}
